package com.duanzhixing.kafkastudy.consumer;

import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

/**
 * @author duanzhixing
 *
 *消费者自动提交offset
 */
public class Myconsumer
{

    public static void main(String[] args)
    {
        Properties props = new Properties();
        //kafka 集群，broker-list
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.40.102:9092");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        //添加消费者组
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "myconsumer");
        //设置自动提交，默认也是true
        props.put("enable.auto.commit", "true");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
        //这里可以订阅多个主题
        consumer.subscribe(Arrays.asList("first"));

        while (true)
        {
            //这里指定，如果topic里没有可消费的数据，则会暂停10毫秒再去检查，避免长会话不停的轮询消耗资源
            ConsumerRecords<String, String> poll = consumer.poll(10);
            for (ConsumerRecord<String, String> record : poll)

                //打印偏移量和数据的kv
                System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
        }

    }
}
