package com.duanzhixing.kafkastudy.consumer;

import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

/**
 * @author duanzhixing
 *手动提交offset,同步提交方式，这种属于阻塞提交
 */
public class Myconsumer2
{

    public static void main(String[] args)
    {

        Properties props = new Properties();
        //kafka 集群，broker-list
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.40.102:9092");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        //添加消费者组
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "myconsumer");
        //设置自动提交，默认也是false
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
        //这里可以订阅多个主题
        consumer.subscribe(Arrays.asList("first"));

        while (true)
        {
            //这里指定，如果topic里没有可消费的数据，则会暂停10毫秒再去检查，避免长会话不停的轮询消耗资源
            ConsumerRecords<String, String> poll = consumer.poll(10);
            for (ConsumerRecord<String, String> record : poll)
            {

                //打印偏移量和数据的kv
                System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
            }

            //同步提交，当前线程会阻塞直到 offset 提交成功
            consumer.commitSync();
        }

    }
}
