package com.duanzhixing.kafkastudy.consumer;

import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.TopicPartition;

/**
 * @author duanzhixing
 *消费者手动提交offset，异步提交
 */
public class Myconsumer3
{

    public static void main(String[] args)
    {

        Properties props = new Properties();
        //kafka 集群，broker-list
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.40.102:9092");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        //添加消费者组
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "myconsumer");
        //设置自动提交，默认也是false
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
        //这里可以订阅多个主题
        consumer.subscribe(Arrays.asList("first"));

        while (true)
        {
            //这里指定，如果topic里没有可消费的数据，则会暂停10毫秒再去检查，避免长会话不停的轮询消耗资源
            ConsumerRecords<String, String> poll = consumer.poll(10);
            for (ConsumerRecord<String, String> record : poll)
            {

                //打印偏移量和数据的kv
                System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
            }

            //异步提交
            consumer.commitAsync(new OffsetCommitCallback()
            {

                public void onComplete(Map<TopicPartition, OffsetAndMetadata> offsets, Exception exception)
                {
                    //如果提交失败则打印下哪个提交失败了
                    if (exception != null)
                    {
                        System.err.println("Commit  failed  for" + offsets);
                    }
                }
            });
        }

    }
}
