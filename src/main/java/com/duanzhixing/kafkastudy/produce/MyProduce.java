package com.duanzhixing.kafkastudy.produce;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

/**
 * @author duanzhixing
 *默认的是异步发送数据，这样的好处是能够增加生产者的发送数据的吞吐量
 */
public class MyProduce
{
    public static void main(String[] args) throws InterruptedException, ExecutionException
    {
        Properties props = new Properties();
        //kafka 集群，broker-list
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.40.102:9092");
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        Producer<String, String> producer = new KafkaProducer<String, String>(props);
        for (int i = 0; i < 10; i++)
        {
            RecordMetadata recordMetadata = producer.send(new ProducerRecord<String, String>("first", "duanxzhxing" + i)).get();

            if (recordMetadata != null)
            {
                System.out.println("同步调用" + recordMetadata.offset() + recordMetadata.partition());
                Thread.currentThread().sleep(2000);

            }

        }
        producer.close();

    }
}
