package com.duanzhixing.kafkastudy.produce;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

/**
 * 测试kafka的事务
 * @author duanzhixing
 *
 */
public class MyProduceTransaction
{

    public static void main(String[] args)
    {
        Properties props = new Properties();
        //kafka 集群，broker-list
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.40.101:9092");
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        //需要开启kafka的事务
        props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
        props.setProperty("transactional.id", "test2"); // 事务ID
        Producer<String, String> producer = new KafkaProducer<String, String>(props);
        producer.initTransactions();//初始化事务
        try
        {
            //开启一个事物
            producer.beginTransaction();
            //这里我们故意造成一个程序异常，让事务起作用，数据提交不上去
            int i = 1 / 0;
            RecordMetadata recordMetadata = producer.send(new ProducerRecord<String, String>("testta", "duanxzhxing")).get();

            //提交事务
            producer.commitTransaction();
        }
        catch (Exception e)
        {
            //事物回滚
            producer.abortTransaction();
            e.printStackTrace();
        }
        producer.close();
    }
}
