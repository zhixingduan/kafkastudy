package com.duanzhixing.kafkastudy.produce.interceptor;

import java.util.Map;

import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class MyInterceptor1 implements ProducerInterceptor<String, String>
{

    public void configure(Map<String, ?> configs)
    {
        // TODO Auto-generated method stub

    }

    public ProducerRecord<String, String> onSend(ProducerRecord<String, String> record)
    {
        // TODO Auto-generated method stub
        return new ProducerRecord<String, String>(record.topic(), String.valueOf(Integer.valueOf(record.value()) + 10));
    }

    public void onAcknowledgement(RecordMetadata metadata, Exception exception)
    {
        // TODO Auto-generated method stub

    }

    public void close()
    {
        // TODO Auto-generated method stub

    }

}
