package com.duanzhixing.kafkastudy.produce.interceptor;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

/**
 * @author duanzhixing
 *
 */
public class TestInterceptor
{

    public static void main(String[] args) throws InterruptedException, ExecutionException
    {
        Properties props = new Properties();
        //kafka 集群，broker-list
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.40.101:9092");
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        //配置拦截器，拦截器的顺序就是数据过滤的顺序
        List<String> interceptors = new ArrayList<String>();
        interceptors.add("com.duanzhixing.kafkastudy.produce.interceptor.MyInterceptor1"); // interceptor 1
        interceptors.add("com.duanzhixing.kafkastudy.produce.interceptor.MyInterceptor2"); // interceptor 2
        props.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG, interceptors);

        Producer<String, String> producer = new KafkaProducer<String, String>(props);

        RecordMetadata recordMetadata = producer.send(new ProducerRecord<String, String>("testta", "1")).get();

        producer.close();
    }
}
