package com.duanzhixing.kafkastudy.produce;

import java.util.Map;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

/**
 * @author duanzhixing
 *生产者模块自定义分区器
 */
public class MyPartiton implements Partitioner
{

    public void configure(Map<String, ?> configs)
    {
        // TODO Auto-generated method stub

    }

    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster)
    {
        //这里固定一个0分区，可以根据自己业务进行分区处理
        return 0;
    }

    public void close()
    {
        // TODO Auto-generated method stub

    }

}
